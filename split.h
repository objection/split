
#include <stdbool.h>
#include <stdlib.h>

char **split (const char *s, const char *e, const char **delims, size_t n_delims,
		size_t *n_res, bool squeeze_empty);
char *join (const char **strs, int n_strs, const char *gap);
