# Split

A split function. It works "the obvious way", and it seems fast
enough.

You can provide with multiple delimiters, which are null-terminated
strings.

Also: don't use this. Pretty buggy, according to clang.
