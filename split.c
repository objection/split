#define _GNU_SOURCE
#include "split.h"
#include <string.h>
#include <stdio.h>

#define DECLARE_ARR(type, libname, name) \
		typedef struct libname##_##name##_arr { \
			type *d; \
			size_t a, n; \
		} libname##_##name##_arr; \
		libname##_##name##_arr libname##_create_##name##_arr (size_t init_size); \
		int libname##_push_##name##_arr (libname##_##name##_arr *s, type item); \
		void libname##_remove_from_##name##_arr (libname##_##name##_arr *s, size_t where, \
				size_t n); \

#define DEFINE_ARR(type, libname, name) \
		libname##_##name##_arr libname##_create_##name##_arr(size_t init_size) { \
			libname##_##name##_arr res; \
			if (!init_size) init_size = 4; \
			res.d = malloc (sizeof(type) * init_size); \
			if (!res.d) { \
				free (res.d); \
				res.a = -1; \
			} else { \
				res.n = 0, res.a = init_size; \
			} \
			return res; \
		} \
		int libname##_push_##name##_arr(libname##_##name##_arr *s, type item) { \
			type *tmp; \
			if ((*s).n >= (*s).a) { \
				tmp = realloc ((*s).d, ((*s).a * 2) * sizeof (type)); \
				if (!tmp) return -1;  \
				(*s).d = tmp; \
				(*s).a *= 2; \
			} \
			(*s).d[(*s).n++] = item; \
			return (*s).n; \
		} \
		void libname##_remove_from_##name##_arr (libname##_##name##_arr *s, size_t where, \
				size_t _n) { \
			 memmove ((*s).d + where, (*s).d + where + _n, \
					 ((*s).n - where - _n) * sizeof *(*s).d); \
			(*s).n -= _n; \
		} \

DECLARE_ARR (char *, split, str);
DEFINE_ARR (char *, split, str);
DECLARE_ARR (size_t, split, size_t);
DEFINE_ARR (size_t, split, size_t);

static bool split_is_null_byte (const char *p, const char *e) {
	(void) e;
	if (*p == '\0') return 1; else return 0;
}

static bool split_is_e (const char *p, const char *e) {
	if (p == e) return 1; else return 0;
}

// Returns the index of a matching delim, else -1.
static int split_match_index (const char **delims, size_t n_delims, const char *p,
		size_t *lens) {
	int res = -1;
	for (const char **delim = delims; delim < delims + n_delims; delim++) {
		if (!strncmp (p, *delim, lens[delim - delims])) {
			res = delim - delims;
			break;
		}
	}
	return res;
}

// Returns 1 if it encounters the end of the string.
static int split_skip_repeated_matches (const char **delims, size_t n_delims,
		const char **p, const char *e, size_t *lens) {
	(void) e;
	int delim_match_index = -1;
	int res = 0;
	while ((delim_match_index = split_match_index (delims, n_delims, *p,
					lens)) != -1) {
		*p += lens[delim_match_index];
		res++;
	}
	return res;
	/* if (end_of_string (*p, e)) return 1; */
	/* return 0; */
}

static void split_push_blanks (struct split_str_arr *arr, int n) {
	for (int i = 0; i < n; i++) {
		char *str = malloc (1);
		*str = '\0';
		split_push_str_arr (arr, str);
	}
}

// Returns 1 for end of string
static int split_squeeze_or_split_push_blanks (const char **p, const char *e,
		const char **delims,
		size_t n_delims, struct split_size_t_arr *lens, bool squeeze_empty,
		struct split_str_arr *res, bool (*end_of_string) (const char *, const char *)) {

	int n_blanks = split_skip_repeated_matches (delims, n_delims,
		p, e, (*lens).d);
	if (!squeeze_empty) split_push_blanks (res, n_blanks - 1);
	if (end_of_string (*p, e)) return 1;
	return 0;
}

// s is the start of the string, e is the end. If you pass NULL to e, the end
// of the string is '\0'.
char **split (const char *s, const char *e, const char **delims, size_t n_delims, size_t *n_res,
		bool squeeze_empty) {

	bool (*end_of_string) (const char *, const char *) = e
		? split_is_e
		: split_is_null_byte;

	struct split_size_t_arr lens = split_create_size_t_arr (n_delims);
	for (const char **delim = delims; delim < delims + n_delims; delim++)
		split_push_size_t_arr (&lens, strlen (*delim));

	const char *p = s, *last = s;
	struct split_str_arr res = split_create_str_arr (8);
	if (split_squeeze_or_split_push_blanks (&p, e, delims, n_delims, &lens,
			squeeze_empty, &res, end_of_string)) {
		*n_res = res.n;
		return !*n_res? NULL: res.d;
	}
	last = p;
	int delim_match_index;
	bool end = 0;
	for (;;) {
		if ((end = end_of_string (p, e))
					|| (delim_match_index = split_match_index (delims, n_delims,
							p, lens.d)) != -1) {
			char *str = NULL;
			asprintf (&str, "%.*s", (int) (p - last), last);
			split_push_str_arr (&res, str);
			if (end) break;
			if (split_squeeze_or_split_push_blanks (&p, e, delims, n_delims, &lens,
					squeeze_empty, &res, end_of_string)) {
				break;
			}

			last = p;
		} else p++;
	}
	free (lens.d);
	*n_res = res.n;
	res.d = realloc (res.d, res.n * sizeof *res.d);
	return res.d;
}

size_t split_count_up_strs (const char **strs, int n) {
	/* doesn't add a '\0' byte */
	size_t len = 0;
	for (int i = 0; i < n; i++) {
		if (!strs[i]) continue;
		len += strlen (strs[i]);
	}
	return len;
}

char *join (const char **strs, int n_strs, const char *gap) {

	// Might be mariginally faster to keep an array of the string lens,
	// since I strlen later.
	size_t n_buf = split_count_up_strs (strs, n_strs);
	size_t n_gap = strlen (gap);
	n_buf += n_gap * n_strs;
	char *buf = malloc (n_buf + 1);
	if (!buf) return NULL;
	char *pbuf = buf;
	size_t len;
	for (int i = 0; i < n_strs; i++) {
		pbuf = memcpy (pbuf, strs[i], len = strlen (strs[i]));
		pbuf += len;
		if (i != n_strs - 1) {
			pbuf = memcpy (pbuf, gap, n_gap);
			pbuf += n_gap;
		}
	}
	*pbuf = '\0';
	return buf;
}
